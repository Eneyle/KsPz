# app.py
import http.server
import socketserver
import urllib.parse
import json
import random
import redis

# Подключение к Redis
redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

# Функция бинарного поиска
def binary_search(arr, target):
    left = 0
    right = len(arr) - 1
    while left <= right:
        mid = (left + right) // 2
        if arr[mid] == target:
            return mid
        if arr[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1

# Обработчик HTTP-запросов
class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        parsed_url = urllib.parse.urlparse(self.path)
        query_params = urllib.parse.parse_qs(parsed_url.query)

        if 'target' not in query_params:
            self.send_response(400)
            self.end_headers()
            self.wfile.write(b'Missing "target" parameter')
            return

        target = int(query_params['target'][0])
        arr = redis_client.get('array')
        if not arr:
            arr = generate_and_store_array()
        else:
            arr = eval(arr)

        index = binary_search(arr, target)
        
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps({'index': index}).encode())

# Генерация и сохранение массива в Redis
def generate_and_store_array():
    arr = sorted([random.randint(1, 100) for _ in range(100)])
    redis_client.set('array', str(arr))
    return arr

if __name__ == '__main__':
    PORT = 8080
    with socketserver.TCPServer(("", PORT), RequestHandler) as httpd:
        print("Serving at port", PORT)
        httpd.serve_forever()
