# app_test.py
import unittest
from app import binary_search

class TestBinarySearch(unittest.TestCase):
    def test_existing_value(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 3), 2)

    def test_non_existing_value(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 6), -1)

    def test_boundary_values(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 1), 0)
        self.assertEqual(binary_search(arr, 5), 4)

    def test_empty_array(self):
        arr = []
        self.assertEqual(binary_search(arr, 1), -1)

    def test_invalid_input(self):
        arr = [1, 2, 3, 4, 5]
        with self.assertRaises(TypeError):
            binary_search(arr, 'a')

if __name__ == '__main__':
    unittest.main()
