# Dockerfile
FROM python:3

RUN pip install redis

COPY app.py /app/

WORKDIR /app

CMD ["python", "app.py"]
